package mathiashh.dk.squareretrofitexample.json;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import mathiashh.dk.squareretrofitexample.R;
import mathiashh.dk.squareretrofitexample.json.w3schools.BreakfastMenu;
import mathiashh.dk.squareretrofitexample.json.brondby.Channel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * see something like https://futurestud.io/blog/retrofit-synchronous-and-asynchronous-requests
 * and https://github.com/codepath/android_guides/wiki/Consuming-APIs-with-Retrofit
 * http://www.utilities-online.info/xmltojson/ seems like this one sucks
 * try this one https://www.freeformatter.com/xml-to-json-converter.html
 *
 * Use  http://www.jsonschema2pojo.org/ to convert the json to pojo
 */

public class JsonMainActivity extends AppCompatActivity {


    static BreakfastMenu breakfastMenu;
    static Channel bifRss;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    private void fetchW3SchoolsJson() {
        ApiService apiService = getApiServiceWithURL(" https://bitbucket.org/");
        Call<BreakfastMenu> menuCall = apiService.getBreakfastMenuJson();
        menuCall.enqueue(new Callback<BreakfastMenu>() {
            @Override
            public void onResponse(Call<BreakfastMenu> call, Response<BreakfastMenu> menuResponse) {
                BreakfastMenu breakfastMenu = menuResponse.body();
                JsonMainActivity.breakfastMenu = breakfastMenu;
            }

            @Override
            public void onFailure(Call<BreakfastMenu> call, Throwable t) {

            }
        });
    }


    private void fetchBrondbyJson() {
        ApiService apiService = getApiServiceWithURL(" https://bitbucket.org/");
        Call<Channel> channelCall = apiService.getBrondbyNewsJson();
        channelCall.enqueue(new Callback<Channel>() {
            @Override
            public void onResponse(Call<Channel> call, Response<Channel> channelResponse) {
                Channel channel = channelResponse.body();
                JsonMainActivity.bifRss = channel;

            }


            @Override
            public void onFailure(Call<Channel> call, Throwable t) {

            }
        });
    }


    private static ApiService getApiServiceWithURL(String baseUrl) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
//                .addConverterFactory(SimpleXmlConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiService apiService = retrofit.create(ApiService.class);
        return apiService;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_w3schools) {
            fetchW3SchoolsJson();
            return true;
        }
       else if (id == R.id.action_brondby) {
            fetchBrondbyJson();
        }

        return super.onOptionsItemSelected(item);
    }


}
