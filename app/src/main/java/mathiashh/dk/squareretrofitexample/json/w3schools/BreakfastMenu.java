package mathiashh.dk.squareretrofitexample.json.w3schools;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;


public class BreakfastMenu {

    @SerializedName("breakfast_menu")
    @Expose
    private BreakfastMenu_ breakfastMenu;

    public BreakfastMenu_ getBreakfastMenu() {
        return breakfastMenu;
    }

    public void setBreakfastMenu(BreakfastMenu_ breakfastMenu) {
        this.breakfastMenu = breakfastMenu;
    }

}


class BreakfastMenu_ {

    @SerializedName("food")
    @Expose
    private List<Food> food = null;

    public List<Food> getFood() {
        return food;
    }

    public void setFood(List<Food> food) {
        this.food = food;
    }

}


class Food {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("calories")
    @Expose
    private String calories;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCalories() {
        return calories;
    }

    public void setCalories(String calories) {
        this.calories = calories;
    }

}
