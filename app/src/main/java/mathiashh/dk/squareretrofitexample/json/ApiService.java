package mathiashh.dk.squareretrofitexample.json;

import mathiashh.dk.squareretrofitexample.json.w3schools.BreakfastMenu;
import mathiashh.dk.squareretrofitexample.json.brondby.Channel;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;


/**
 * Created by mhh on 19/12/15.
 */
public interface ApiService {


    @GET("/MathiasHH/squareretrofitexample/raw/cbea48437d1cd0f37b8fa6940172cd52765f3b21/app/src/xmAndJsonlFilesToParse/w3schools.com/simple.json/")
    Call<ResponseBody> getBreakfastMenuJsonResponseBody();

    @GET("/MathiasHH/squareretrofitexample/raw/cbea48437d1cd0f37b8fa6940172cd52765f3b21/app/src/xmAndJsonlFilesToParse/w3schools.com/simple.json/")
    Call<BreakfastMenu> getBreakfastMenuJson();

    @GET("/MathiasHH/squareretrofitexample/raw/cbea48437d1cd0f37b8fa6940172cd52765f3b21/app/src/xmAndJsonlFilesToParse/brondby.com/rss.json/")
    Call<Channel> getBrondbyNewsJson();

}
