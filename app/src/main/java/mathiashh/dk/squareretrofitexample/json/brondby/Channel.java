
        package mathiashh.dk.squareretrofitexample.json.brondby;

        import java.util.List;
        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

public class Channel {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("link")
    @Expose
    private List<String> link = null;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("pubDate")
    @Expose
    private String pubDate;
    @SerializedName("item")
    @Expose
    private List<Item> item = null;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getLink() {
        return link;
    }

    public void setLink(List<String> link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public List<Item> getItem() {
        return item;
    }

    public void setItem(List<Item> item) {
        this.item = item;
    }

}

//        package mathiashh.dk.squareretrofitexample.json.brondby;
//        import com.google.gson.annotations.Expose;
//        import com.google.gson.annotations.SerializedName;
//public
class Enclosure {

    @SerializedName("@url")
    @Expose
    private String url;
    @SerializedName("@length")
    @Expose
    private String length;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

}
//        package mathiashh.dk.squareretrofitexample.json.brondby;
//        import com.google.gson.annotations.Expose;
//        import com.google.gson.annotations.SerializedName;
//public
class Guid {

    @SerializedName("@isPermaLink")
    @Expose
    private String isPermaLink;
    @SerializedName("#text")
    @Expose
    private String text;

    public String getIsPermaLink() {
        return isPermaLink;
    }

    public void setIsPermaLink(String isPermaLink) {
        this.isPermaLink = isPermaLink;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}

//        package mathiashh.dk.squareretrofitexample.json.brondby;
//        import com.google.gson.annotations.Expose;
//        import com.google.gson.annotations.SerializedName;
//public
class Item {

    @SerializedName("guid")
    @Expose
    private Guid guid;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("enclosure")
    @Expose
    private Enclosure enclosure;
    @SerializedName("pubDate")
    @Expose
    private String pubDate;

    public Guid getGuid() {
        return guid;
    }

    public void setGuid(Guid guid) {
        this.guid = guid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Enclosure getEnclosure() {
        return enclosure;
    }

    public void setEnclosure(Enclosure enclosure) {
        this.enclosure = enclosure;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

}
//        package mathiashh.dk.squareretrofitexample.json.brondby;
//        import com.google.gson.annotations.Expose;
//        import com.google.gson.annotations.SerializedName;

class Version {

    @SerializedName("@version")
    @Expose
    private String version;
    @SerializedName("channel")
    @Expose
    private Channel channel;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

}