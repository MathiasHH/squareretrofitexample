package mathiashh.dk.squareretrofitexample.xml.w3schools;


import java.util.ArrayList;
import java.util.List;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name = "breakfast_menu")
public class BreakfastMenu
{
    @ElementList(inline = true)
    protected List<Food> food;

    public List<Food> getConfigurations()
    {
        if (food == null)
        {
            food = new ArrayList<>();
        }
        return this.food;
    }

    public void setConfigurations(List<Food> configuration)
    {
        this.food = configuration;
    }

}