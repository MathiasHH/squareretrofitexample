package mathiashh.dk.squareretrofitexample.xml.w3schools;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by mhh on 19/12/15.
 */
//@Root(name = "items")
@Root(name = "food")
public class Food {
    @Element(name = "name")
    String name;

    @Element(name = "price")
    String price;

    @Element(name = "description")
    String description;

    @Element(name = "calories")
    String calories;
}
