package mathiashh.dk.squareretrofitexample.xml;

import mathiashh.dk.squareretrofitexample.xml.brondby.RSS;
import mathiashh.dk.squareretrofitexample.xml.w3schools.BreakfastMenu;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by mhh on 19/12/15.
 */
public interface ApiService {


    @GET("/xml/simple.xml")
    Call<BreakfastMenu> getBreakfastMenuXML();


    @GET("/rss")
    Call<RSS> getBrondbyNewsXML();

}
