package mathiashh.dk.squareretrofitexample.xml.brondby;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Text;

/**
 * Created by mhh on 19/12/15.
 */
public class Link {
    @Attribute(required=false)
    public String href;

    @Attribute(required=false)
    public String rel;

    @Attribute(name="type",required=false)
    public String contentType;

    @Text(required=false)
    public String link;
}