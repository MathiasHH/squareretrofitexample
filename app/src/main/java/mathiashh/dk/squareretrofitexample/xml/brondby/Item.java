package mathiashh.dk.squareretrofitexample.xml.brondby;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by mhh on 19/12/15.
 */

@Root(name = "items")
public class Item {
    @Element(name = "guid", required = false)String guid;
    @Element(name = "title", required = false)String title;
    @Element(name = "description", required = false)String description;
    @Element(name = "link", required = false)String link;
    @Element(name = "enclosure", required = false)String enclosure;
    @Element(name = "pubDate", required = false)String pubDate;
}

