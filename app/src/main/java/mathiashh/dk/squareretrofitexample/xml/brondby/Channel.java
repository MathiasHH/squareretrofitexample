package mathiashh.dk.squareretrofitexample.xml.brondby;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import java.util.List;

@Root(name = "channel")
public class Channel {
    @Element(name="title", required = false) String title;
//    @Element(name="link", required = false) String link; // this parser is broken so we do this:
    @ElementList(entry="link",inline=true,required=false)
    public List<Link> links; // see http://stackoverflow.com/questions/5973028/simple-xml-element-declared-twice-error
    @Element(name="description", required = false)String description;
    @Element(name="atom:link", required = false)protected String atomlink;
    @Element(name="language", required = false)protected String language;
    @Element(name="pubDate", required = false)protected String pubDate;

    @ElementList(entry = "item", inline = true)
    protected List<Item> items;
}