package mathiashh.dk.squareretrofitexample.xml.brondby;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;


/**
 * Created by mhh on 19/12/15.
 *
 * see http://simple.sourceforge.net/download/stream/doc/tutorial/tutorial.php#list
 */
@Root(name = "rss")
public class RSS {
    @Attribute(name="version")private String version;
    @Element(name="channel", required = false)protected Channel channel;
}
