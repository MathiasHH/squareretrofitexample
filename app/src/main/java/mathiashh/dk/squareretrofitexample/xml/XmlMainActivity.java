package mathiashh.dk.squareretrofitexample.xml;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import java.io.IOException;

import mathiashh.dk.squareretrofitexample.R;
import mathiashh.dk.squareretrofitexample.xml.brondby.RSS;
import mathiashh.dk.squareretrofitexample.xml.w3schools.BreakfastMenu;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;


/**
 * see something like https://futurestud.io/blog/retrofit-synchronous-and-asynchronous-requests
 * and https://github.com/codepath/android_guides/wiki/Consuming-APIs-with-Retrofit
 * http://www.utilities-online.info/xmltojson/
 */

public class XmlMainActivity extends AppCompatActivity {


    static BreakfastMenu breakfastMenu;
    static RSS bifRss;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }


    private void getW3SchoolsXml() {
        ApiService apiService = getApiServiceWithURL("https://www.w3schools.com/");
        Call<BreakfastMenu> menuCall = apiService.getBreakfastMenuXML();
        menuCall.enqueue(new Callback<BreakfastMenu>() {
            @Override
            public void onResponse(Call<BreakfastMenu> call, Response<BreakfastMenu> menuResponse) {
                BreakfastMenu breakfastMenu = menuResponse.body();
                XmlMainActivity.breakfastMenu = breakfastMenu;
            }


            @Override
            public void onFailure(Call<BreakfastMenu> call, Throwable t) {

            }
        });
    }


    private void getBrondbyXml() {
        new BifXmlAsyncTask().execute();
    }

    static class BifXmlAsyncTask extends AsyncTask<Integer, Double, Boolean>
    {
        @Override
        protected Boolean doInBackground(Integer... arg0)
        {
            ApiService apiService = getApiServiceWithURL("http://brondby.com/");
            Call<RSS> rssCall = apiService.getBrondbyNewsXML();
            try {
                Response<RSS> rssResponse = rssCall.execute();// call the network
                RSS bifRss = rssResponse.body();
                XmlMainActivity.bifRss = bifRss;
            } catch (IOException e) {
                e.printStackTrace();
            }

            return true;
        }

        /** Denne metode udf�res af hovedtråden, efter at baggrundstråden er afsluttet */
        @Override
        protected void onPostExecute(Boolean setContentViewAgain) { }
    }


    private static ApiService getApiServiceWithURL(String baseUrl) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();
        ApiService apiService = retrofit.create(ApiService.class);
        return apiService;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_w3schools) {
            getW3SchoolsXml();
            return true;
        } else if (id == R.id.action_brondby) {
            getBrondbyXml();
        }

        return super.onOptionsItemSelected(item);
    }
}
